#include "stdio.h"
#include "stdlib.h"
#include "../include/DataStructure.h"
int main()
{
	/* start always points to the first node of the linked list.
	temp is used to point to the last node of the linked list.*/
	DSIMPL_T start, temp;
	start = createDS();
	temp = start;
	temp->next = NULL;
	/* Here in this code, we take the first node as a dummy node.
	The first node does not contain data, but it used because to avoid handling special cases
	in insert and delete functions.
	*/
	printf("1. Insert\n");
	printf("2. Delete\n");
	printf("3. Print\n");
	printf("4. Find\n");
	while (1)
	{
		int query;
		scanf("%d", &query);
		if (query == 1)
		{
			unsigned int data;
			scanf("%d", &data);
			insertElement(start, data);
		}
		else if (query == 2)
		{
			unsigned int data;
			scanf("%d", &data);
			deleteElement(start, data);
		}
		else if (query == 3)
		{
			printf("The list is ");
			print(start->next);
			printf("\n");
		}
		else if (query == 4)
		{
			unsigned int data;
			scanf("%d", &data);
			int status = searchElement(start, data);
			if (status)
			{
				printf("Element Found\n");
			}
			else
			{
				printf("Element Not Found\n");

			}
		}
		else
			break;
	}


}
