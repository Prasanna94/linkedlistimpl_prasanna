#ifndef _HEADER_H
#define _HEADER_H
#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>
#include "../include/DataStructure.h"
DSIMPL_T createDS()
{
	DSIMPL_T impl = (DSIMPL_T)malloc(sizeof(struct dsImpl));
	return impl;
}
unsigned long long int getCount(DSIMPL_T impl)
{
	unsigned long long int count = 0;
	while (impl->next != NULL)
	{
		count++;
		impl = impl->next;
	}
	return count;
}
int insertElement(DSIMPL_T impl, unsigned int element)
{
	/* Iterate through the list till we encounter the last node.*/
	unsigned long long int count = getCount(impl);
	while (impl->next != NULL)
	{
		impl = impl->next;
	}
	/* Allocate memory for the new node and put data in it.*/
	impl->next = (DSIMPL_T)malloc(sizeof(struct dsImpl));
	impl = impl->next;
	impl->data = element;
	impl->next = NULL;
	if (count == getCount(impl))
		return 0;
	else
		return 1;
}
int searchElement(DSIMPL_T impl, unsigned int element)

{
	impl = impl->next; //First node is dummy node.
	/* Iterate through the entire linked list and search for the key. */
	while (impl != NULL)
	{
		if (impl->data == element) //key is found.
		{
			return 1;
		}
		impl = impl->next;//Search in the next node.
	}
	/*Key is not found */
	return 0;
}
int deleteElement(DSIMPL_T impl, unsigned int element)
{
	/* Go to the node for which the node next to it has to be deleted */
	unsigned long long int count = getCount(impl);
	while (impl->next != NULL && (impl->next)->data != element)
	{
		impl = impl->next;
	}
	if (impl->next == NULL)
	{
		printf("Element %d is not present in the list\n", element);
	}
	/* Now impl points to a node and the node next to it has to be removed */
	DSIMPL_T temp;
	temp = impl->next;
	/*temp points to the node which has to be removed*/
	impl->next = temp->next;
	/*We removed the node which is next to the impl (which is also temp) */
	free(temp);
	/* Beacuse we deleted the node, we no longer require the memory used for it .
	free() will deallocate the memory.
	*/
	if (count = getCount(impl))
		return 0;
	else
		return 1;
}
void deleteDS(DSIMPL_T impl)
{
	DSIMPL_T temp = impl;
	while (impl != NULL)
	{
		temp = impl;
		impl = impl->next;
		free(temp);
	}
}
void print(DSIMPL_T impl)
{
	if (impl == NULL)
	{
		return;
	}
	printf("%d ", impl->data);
	print(impl->next);
}
#endif
