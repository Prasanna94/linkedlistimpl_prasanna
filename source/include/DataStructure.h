struct dsImpl
{
	int data;
	struct dsImpl *next;
};
typedef struct dsImpl* DSIMPL_T;
DSIMPL_T createDS();
unsigned long long int getCount(DSIMPL_T impl);
int insertElement(DSIMPL_T impl, unsigned int element);
int searchElement(DSIMPL_T impl, unsigned int element);
int deleteElement(DSIMPL_T impl, unsigned int element);
void deleteDS(DSIMPL_T impl);
void print(DSIMPL_T impl);

