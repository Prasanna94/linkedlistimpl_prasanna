#include<stdio.h>
#include<stdlib.h>
#include "../include/DataStructure.h"
int testCreateDS()
{
	if (createDS()==NULL)
		return 0;
	else
		return 1;
}
int testGetCount()
{
	DSIMPL_T head=createDS();
	if(head == NULL)
		return 1;
	head->next=NULL;
	if(getCount(head)!=0)
	{
		printf("\nError in the count value for an empty list");
		return 0;
	}
	insertElement(head,1);
	if(getCount(head)!=1)
	{
		printf("\nError in the count value for a list with one 			element");
		return 0;
	}
	insertElement(head,2);
	insertElement(head,3);
	if(getCount(head)!=3)
	{
		printf("\nError in the count value for a list with more than one element");
		return 0;
	}
	deleteDS(head);	
	return 1;
}
int testInsertElement()
{
	DSIMPL_T head = createDS();
	if(head == NULL)
		return 1;
	head->next = NULL;
	if (insertElement(head, 1)!=0)  // testing whether the insert works for a single element
	{
		if (head->next->data != 1)
		{
			printf("Error in inserting the first element");
			return 0;	
		}
		/*else
		{
			printf("passed the testcase for inserting the first element");
		}*/
	}
	if (insertElement(head, 2)!=0) // testing whether the inserts works for more than one element
	{
		if (head->next->next->data != 2)
		{		
			printf("Error in inserting an element");
			return 0;
		}
		/*else
		{
			printf("passed the testcase for inserting the subsequent elements");
		}*/
	}
	deleteDS(head);
	return 1;
}
int testSearchElement()
{
	DSIMPL_T head = createDS();
	if(head == NULL)
		return 1;
	head->next = NULL;
	int i; 
	for (i=1; i <= 10 ; i++) 
		insertElement(head, i);
	if (searchElement(head, 1) != 1) // test case for searching the first element
	{
		printf("Error in searching the first element");
		return 0;	
	}
	if (searchElement(head, 10) != 1) // test case for searching the last element
	{
		printf("Error in searching the last element");
		return 0;	
	}
	if (searchElement(head, 5) != 1) // test case for searching the middle element
	{
		printf("Error in searching the middle element");
		return 0;	
	}
	if (searchElement(head, 15) != 0) // test case for searching the middle element
	{
		printf("Error in searching the element not present in the list");
		return 0;	
	}
	deleteDS(head);
	return 1;
}
int testDeleteElement()
{
	DSIMPL_T head = createDS();
	if(head == NULL)
		return 1;
	head->next = NULL;
	int i; 
	for (i=1; i <= 10 ; i++) 
		insertElement(head, i);
	deleteElement(head, 1);  // test case for deleting the first element
	if (searchElement(head, 1) != 0)
	{
		printf("Error in deleting the first element");
		return 0;	
	}
	deleteElement(head, 10);  // test case for deleting the first element
	if (searchElement(head, 10) != 0)
	{
		printf("Error in deleting the last element");
		return 0;	
	}
	deleteElement(head, 4);  // test case for deleting the middle element
	if (searchElement(head, 4) != 0)
	{
		printf("Error in deleting the middle element");
		return 0;	
	}
	/*if (deleteElement(head, 15)) // test case for deleting the element not present in the list
	{
		printf("Error in deleting the element not present in the list");
		return 0;	
	}
	deleteDS(head);*/
	return 1;
	
}
int testDeleteDS()
{
	DSIMPL_T head = createDS();
	if(head == NULL)
		return 1;
	head->next = NULL;
	insertElement(head, 1);
	deleteDS(head);
	if (head->next != NULL)
	{
		printf("\nError in deleting the linkedlist with one element");
		return 0;
	}
	int i; 
	
	for (i = 1; i <= 10 ; i++) 
		insertElement(head, i);
	deleteDS(head);
	if (head->next != NULL)
	{
		printf("\nError in deleting the linkedlist with more than one element");
		return 0;
	}
	deleteDS(head);
	return 1;
}
int main()
{
	if(testInsertElement()==1)
		printf("\nPASSED INSERT TESTCASE");
	else
		printf("\nFAILED INSERT TESTCASE");
	if(testGetCount()==1)
		printf("\nPASSED GETCOUNT TESTCASE");
	else
		printf("\nFAILED GETCOUNT TESTCASE");
	if(testSearchElement()==1)
		printf("\nPASSED SEARCH TESTCASE");
	else
		printf("\nFAILED SEARCH TESTCASE");
	if(testDeleteElement()==1)
		printf("\nPASSED DELETE TESTCASE");
	else
		printf("\nFAILED DELETE TESTCASE");
	if(testDeleteDS()==1)
		printf("\nPASSED DELETE DS TESTCASE");
	else
		printf("\nFAILED DELETE DS TESTCASE");
	return 0;
}
